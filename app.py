from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
from typing import Dict

app = FastAPI()

class Prodotto(BaseModel):
    id: int
    prodotto: str
    descrizione: str
    prezzo: float
    disponibilità: bool

# dict fittizio in memoria costruito con BaseModel di pydantic
prodotti_db: Dict[int, Prodotto] = {
    1: Prodotto(id=1, prodotto="Classico", descrizione="Panino Classico", prezzo=3.50, disponibilità=True),
    2: Prodotto(id=2, prodotto="Vegetariano", descrizione="Panino con verdure", prezzo=5.00, disponibilità=False),
    3: Prodotto(id=3, prodotto="Crispy Bacon", descrizione="Con hamburger e bacon croccante", prezzo=6.00, disponibilità=True),
}

@app.get("/prodotto/{prodotto_id}", response_model=Prodotto)
async def leggi_prodotto(prodotto_id: int):
    panino = prodotti_db.get(prodotto_id)
    if panino is None:
        raise HTTPException(status_code=404, detail="Prodotto non disponibile")
    return panino

@app.get("/prodotti", response_model=list[Prodotto])
async def get_prodotto():
    return list(prodotti_db.values())

@app.post("/prodotti", response_model=Prodotto)
async def create_panino(panino: Prodotto):
    if panino.id in prodotti_db:
        raise HTTPException(status_code=400, detail="Prodotto già esistente")
    prodotti_db[panino.id] = panino
    return panino

@app.put("/prodotti/{prodotto_id}", response_model=Prodotto)
async def update_prodotto(prodotto_id: int, panino: Prodotto):
    if prodotto_id not in prodotti_db:
        raise HTTPException(status_code=404, detail="Prodotto non Esistente")
    prodotti_db[prodotto_id] = panino
    return panino

@app.delete("/prodotti/{prodotto_id}", response_model=Prodotto)
async def delete_prodotto(prodotto_id: int):
    panino = prodotti_db.pop(prodotto_id, None)
    if panino is None:
        raise HTTPException(status_code=404, detail="Prodotto non trovato")
    return panino
