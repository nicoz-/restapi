import pytest
from fastapi.testclient import TestClient
from app import app

@pytest.fixture
def client():
    return TestClient(app)

def test_create_panino(client):
    # test create panino
    panino_data = {
        "id": 4,
        "prodotto": "Panino Test",
        "descrizione": "Panino di prova",
        "prezzo": 4.50,
        "disponibilità": True,
    }
    response = client.post("/prodotti", json=panino_data)
    assert response.status_code == 200
    assert response.json() == panino_data

def test_read_panino(client):
    # test get di un panino esistente
    panino_id = 1
    response = client.get(f"/prodotto/{panino_id}")
    assert response.status_code == 200
    assert response.json()["id"] == panino_id

def test_read_nonesistente_panino(client):
    # test per get panino non esistente
    panino_id = 999
    response = client.get(f"/prodotto/{panino_id}")
    assert response.status_code == 404

def test_update_panino(client):
    # test per put di un panino esistente
    panino_id = 1
    updated_panino_data = {
        "id": panino_id,
        "prodotto": "Panino Modificato",
        "descrizione": "Descrizione modificata",
        "prezzo": 5.00,
        "disponibilità": False,
    }
    response = client.put(f"/prodotti/{panino_id}", json=updated_panino_data)
    assert response.status_code == 200
    assert response.json() == updated_panino_data

def test_update_nonesistente_panino(client):
    # test per metodo put per panino non esistente 
    panino_id = 999
    updated_panino_data = {
        "id": panino_id,
        "prodotto": "Panino Modificato",
        "descrizione": "Descrizione modificata",
        "prezzo": 5.00,
        "disponibilità": False,
    }
    response = client.put(f"/prodotti/{panino_id}", json=updated_panino_data)
    assert response.status_code == 404

def test_delete_panino(client):
    # test metodo delete
    panino_id = 1
    response = client.delete(f"/prodotti/{panino_id}")
    assert response.status_code == 200
    assert response.json()["id"] == panino_id

def test_delete_nonesistente_panino(client):
    # test per la cancellazione di un panino non esistente
    panino_id = 999
    response = client.delete(f"/prodotti/{panino_id}")
    assert response.status_code == 404
